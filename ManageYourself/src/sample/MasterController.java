package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controller.PSController;

import java.io.IOException;

/**
 * Created by Timo on 07.06.16.
 */
public class MasterController {

    private Stage primaryStage;
    private Scene primaryScene;
    private Scene secondaryScene;


    /**
     * Constructor Area
     *
     * @param stage
     */
    public MasterController(Stage stage) throws IOException {

        this.primaryStage = stage;

        setUpPrimaryScene();
        setUpSecondaryScene();
        init();

        primaryStage.show();
    }

    private void init() {
        primaryStage.setScene(primaryScene);
    }


    /**
     * Setting up Scenes
     */
    private void setUpPrimaryScene() throws IOException {
        Parent root = null;


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/sample.fxml"));
        root = loader.load();
        primaryScene = new Scene(root);

        PSController controller = loader.getController();
        controller.setMasterController(this);
    }


    private void setUpSecondaryScene() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/sampleTwo.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        secondaryScene = new Scene(root);
    }


    public void setSceondary() {
        primaryStage.setScene(secondaryScene);
    }
}
