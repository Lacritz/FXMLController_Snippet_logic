package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import sample.MasterController;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Timo on 07.06.16.
 */
public class PSController implements Initializable {

    private MasterController masterController;


    @FXML
    Button button;

    @FXML
    public void setSecondary() {
        masterController.setSceondary();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }
}
